% Create info file for PLS analysis

% for ER-behavioral PLS: include AMF, relevant DDM parameters

clear all; clc;

%% get summary data

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat'], 'STSWD_summary')

%% YA:

% N = 42 YA, 1125, 1214 excluded: no EEG data; 1151,1252,1228,1247 excluded before, not here
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

numSubs = numel(find(ismember(STSWD_summary.IDs,IDs)));
IDs = IDs(ismember(IDs,STSWD_summary.IDs));
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['meanBOLD_ER_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        % add spectral EEG PLS
        dataForPLS{(indCond-1)*numSubs+indID,1} = STSWD_summary.EEG_LV1.slope(curID);
        % add DDM results
        dataForPLS{(indCond-1)*numSubs+indID,2} = STSWD_summary.HDDM_vt.nondecisionMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,3} = STSWD_summary.HDDM_vt.nondecisionMRI_linear(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,4} = STSWD_summary.HDDM_vt.driftMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,5} = STSWD_summary.HDDM_vt.driftMRI_linear(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,6} = STSWD_summary.HDDM_vt.thresholdMRI(curID,1);
        % add 1/f
        dataForPLS{(indCond-1)*numSubs+indID,7} = STSWD_summary.spectralslope_linearChange(curID,1);
        % add entropy
        dataForPLS{(indCond-1)*numSubs+indID,8} = STSWD_summary.MSE_slope(curID,1);
        % add pupil (different baselines)
        dataForPLS{(indCond-1)*numSubs+indID,9} = STSWD_summary.pupil2.stimdiff_slope(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,10} = STSWD_summary.pupil2.stimraw_slope(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,11} = STSWD_summary.pupil2.stimbl_slope(curID,1);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

% normalize each variable: convert to z-scores
dataForPLS_z = [dataForPLS(:,1), num2cell(zscore(cell2mat(dataForPLS(:,2:end))))];

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS_z;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, ...
        Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6}, Fillin.VALUE{indSub,7},...
        Fillin.VALUE{indSub,8}, Fillin.VALUE{indSub,9}, Fillin.VALUE{indSub,10},...
        Fillin.VALUE{indSub,11});
end

% figure;imagesc(cell2mat(dataForPLS_z(:,2:end)))
% figure;imagesc(corrcoef(cell2mat(dataForPLS_z(:,2:end))))
% 
% [lv, coeff] = pca(cell2mat(dataForPLS_z(:,2:end)));
% figure; imagesc(lv)

