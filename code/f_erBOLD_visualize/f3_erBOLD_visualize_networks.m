pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));

% N = 42 YA, 1125, 1214 excluded: no EEG data; 1247 as EEG outlier; 1151,1252,1228,1247 excluded before, not here
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% load common coordinates
load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/VoxelOverlap/coords_N95.mat'], 'final_coords');
%final_coords = final_coords_withoutZero;

%% loop across masks

networks = {'anterior_Salience'; 'Auditory'; 'Basal_Ganglia'; 'dDMN'; 'high_Visual'; 'Language'; ...
    'LECN'; 'post_Salience'; 'Precuneus'; 'prim_Visual'; 'RECN'; 'Sensorimotor'; 'vDMN'; 'Visuospatial'};

for indMask = 1:numel(networks)

    mask = ['/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/Z_parcellations/B_data/A_standards/Shirer_14networks/',networks{indMask},'_MNI_3mm.nii.gz'];
    [img] = double(S_load_nii_2d( mask ));
    img = img(final_coords,:); %restrict to final_coords

    BOLD_network = [];
    for indID = 1:numel(IDs)
        subjData = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/',...
            'meanBOLD_ER_',IDs{indID},'_BfMRIsessiondata.mat']);
        % reshape
        curData = reshape(subjData.st_datamat, 9, 17,numel(subjData.st_coords));
        BOLD_network(indID,:,:) = squeeze(nanmean(curData(:,:,logical(img)),3));
    end

    %% baseline with prestim period

    TRtime = .645.*([1:size(BOLD_thalamus,3)]-1); % minus 1 here, as the first TR captures the cue onset
    idxTR = find(TRtime > 3 & TRtime < 5); % use TRs between 3 and 5 seconds as baseline
    baseline = repmat(nanmean(BOLD_thalamus(:,:,idxTR),3),1,1,size(BOLD_thalamus,3));
    BOLD_thalamus = (BOLD_thalamus-baseline);

    %% median split network response according to drift

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

    idx_AttFactor = ismember(STSWD_summary.IDs,IDs);
    [sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftMRI_linear(idx_AttFactor), 'descend'); % here it is a negative slope

    lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
    highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

    % add within-subject error bars
    pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);

    h = figure('units','normalized','position',[.1 .1 .7 .25]);

        subplot(1,3,1); cla; hold on;
        curData = squeeze(BOLD_network(highChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '--','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', '--', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        title({[networks{indMask},' modulation by drift mod.']}); xlim([1 16])
        legend([l1.mainLine, l3.mainLine],{'High modulators'; 'Low modulators'}, 'location', 'NorthWest'); legend('boxoff');
        xlabel('Time (s)'); ylabel('BOLD magnitude (normalized)');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)

    subplot(1,3,2); cla; hold on;
        curData = squeeze(BOLD_network(lowChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.8 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,2,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.6 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,3,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.4 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.2 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'L1'; 'L2'; 'L3'; 'L4'}, 'location', 'NorthWest'); legend('boxoff');
        title({'Low modulators'}); xlim([1 16]);%ylim([-1 8]);
        xlabel('Time (s)'); ylabel('BOLD magnitude (normalized)');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)

    subplot(1,3,3); cla; hold on;
        curData = squeeze(BOLD_network(highChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,2,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,3,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'L1'; 'L2'; 'L3'; 'L4'}, 'location', 'NorthWest'); legend('boxoff');
        title({'High modulators'}); xlim([1 16]); %ylim([-1 8]);
        xlabel('Time (s)'); ylabel('BOLD magnitude (normalized)');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/C_figures/Z_ER_BOLD_networks/';
    figureName = [networks{indMask}];

    saveas(h, [pn.plotFolder, figureName], 'png');
    close(h);
end

%% perform the same analysis for different thalamic nuclei

% load common coordinates
load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/VoxelOverlap/coords_N95.mat'], 'final_coords');

nuclei = {'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'; 'VP'; 'AllNuclei'};

for indMask = 1:numel(nuclei)

    mask = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/Morel/',nuclei{indMask},'_thr_MNI_3mm.nii.gz'];
    [img] = double(S_load_nii_2d( mask ));
    img = img(final_coords,:); %restrict to final_coords

    BOLD_network = [];
    for indID = 1:numel(IDs)
        subjData = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/',...
            'meanBOLD_ER_',IDs{indID},'_BfMRIsessiondata.mat']);
        % reshape
        curData = reshape(subjData.st_datamat, 9, 17,numel(subjData.st_coords));
        BOLD_network(indID,:,:) = squeeze(nanmean(curData(:,:,logical(img)),3));
    end

%     TRtime = .645.*([1:size(BOLD_network,3)]-1); % minus 1 here, as the first TR captures the cue onset
%     idxTR = find(TRtime > 3 & TRtime < 5); % use TRs between 4 and 6 seconds as baseline
%     baseline = repmat(nanmean(BOLD_network(:,:,idxTR),3),1,1,size(BOLD_network,3));

    % baseline with prestim period:
    baseline = repmat(nanmean(BOLD_network(:,:,4:8),3),1,1,size(BOLD_network,3));
    BOLD_network = (BOLD_network-baseline);

    %% median split thalamic response according to drift

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

    idx_AttFactor = ismember(STSWD_summary.IDs,IDs);
    [sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftMRI_linear(idx_AttFactor), 'descend'); % here it is a negative slope

    lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
    highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

    % add within-subject error bars
    pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);

    h = figure('units','normalized','position',[.1 .1 .7 .25]);

        subplot(1,3,1); cla; hold on;
        curData = squeeze(BOLD_network(highChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '--','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', '--', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        title({[nuclei{indMask},' modulation by drift mod.']}); xlim([1 16])
        legend([l1.mainLine, l3.mainLine],{'High modulators'; 'Low modulators'}, 'location', 'NorthWest'); legend('boxoff');
        xlabel('Time (s)'); ylabel('BOLD magnitude (normalized)');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)

    subplot(1,3,2); cla; hold on;
        curData = squeeze(BOLD_network(lowChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.8 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,2,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.6 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,3,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.4 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(lowChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [.2 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'L1'; 'L2'; 'L3'; 'L4'}, 'location', 'NorthWest'); legend('boxoff');
        title({'Low modulators'}); xlim([1 16]);%ylim([-1 8]);
        xlabel('Time (s)'); ylabel('BOLD magnitude (normalized)');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)

    subplot(1,3,3); cla; hold on;
        curData = squeeze(BOLD_network(highChIdx,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,2,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,3,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_network(highChIdx,4,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar([],nanmean(curData,1),standError, 'lineprops', {'color', [1 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'L1'; 'L2'; 'L3'; 'L4'}, 'location', 'NorthWest'); legend('boxoff');
        title({'High modulators'}); xlim([1 16]); %ylim([-1 8]);
        xlabel('Time (s)'); ylabel('BOLD magnitude (normalized)');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/C_figures/Z_ER_BOLD_nuclei/';
    figureName = [nuclei{indMask}];

    saveas(h, [pn.plotFolder, figureName], 'png');
    close(h);
end