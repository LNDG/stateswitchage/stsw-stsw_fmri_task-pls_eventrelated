% visualize thalamic activity

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.shadedError = fullfile(rootpath, 'tools', 'shadedErrorBar'); addpath(pn.shadedError);
pn.plotFolder = fullfile(rootpath, 'figures');

% N = 42 YA, 1125, 1214 excluded: no EEG data; 1247 as EEG outlier
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};


mask = fullfile(rootpath, '..', 'i_thalamic_nuclei', 'data', 'A_standards', 'Morel', 'AllNuclei_thr_MNI_3mm.nii');
[img] = double(S_load_nii_2d(mask));

BOLD_thalamus = [];
for indID = 1:numel(IDs)
    subjData = load(fullfile(rootpath,'data','MeanBOLD_ER_PLS_v4',...
        ['meanBOLD_ER_',IDs{indID},'_BfMRIsessiondata.mat']));
    % reshape
    curData = reshape(subjData.st_datamat, 9, 17,numel(subjData.st_coords));
    BOLD_thalamus(indID,:,:) = squeeze(nanmean(curData(:,:,logical(img(subjData.st_coords))),3));
end

%% baseline with prestim period

TRtime = .645.*([1:size(BOLD_thalamus,3)]-1); % minus 1 here, as the first TR captures the cue onset
idxTR = find(TRtime > 3 & TRtime < 5); % use TRs between 3 and 5 seconds as baseline
baseline = repmat(nanmean(BOLD_thalamus(:,:,idxTR),3),1,1,size(BOLD_thalamus,3));
BOLD_thalamus = (BOLD_thalamus-baseline);

%% median split thalamic response according to drift

load(fullfile(rootpath, '..','..', 'stsw_multimodal', 'b_crossCorrelations', 'data', 'STSWD_summary.mat'))

idx_AttFactor = ismember(STSWD_summary.IDs,IDs);
[sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftMRI_linear(idx_AttFactor), 'descend'); % here it is a negative slope

lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

time = [0:16].*.645;

h = figure('units','normalized','position',[.1 .1 .6 .25]);
    subplot(1,3,1); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(highChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '--','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', '--', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    title({'Thalamic modulation by load';'split by drift modulation'}); xlim([0 10])
    leg = legend([l1.mainLine, l3.mainLine],{'High drift mod.'; 'Low drift mod.'}, 'location', 'NorthWest'); legend('boxoff');
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,2); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(lowChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.6 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.4 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.2 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'Low modulators'}); xlim([0 10]);ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,3); cla; hold on;
     % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(highChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'High modulators'}); xlim([0 10]); ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(leg,'FontSize',16)

    figureName = 'F_thalamicBOLD_ER';

    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    close(h);