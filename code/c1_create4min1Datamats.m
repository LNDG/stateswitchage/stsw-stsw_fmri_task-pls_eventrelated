% Caluclate PLS relationship between dimensionality changes (4-1) and BOLD
% SD changes(4-1) [behavioral PLS]

% 180323 | adapted by JQK from Steffen's script

clear all; clc; restoredefaultpath;

%pn.root      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/';
pn.root      = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/';
pn.tools	 = [pn.root, 'B6_PLS_eventRelated/T_tools/'];  addpath(genpath(pn.tools));
pn.data      = [pn.root, 'B6_PLS_eventRelated/B_data/'];
pn.matpath   = [pn.data, 'MeanBOLD_ER_PLS_v4/'];

% N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

conditions = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

%% change the datamat information

for indID = 1:(numel(IDs))
    
   disp(['Processing subject ', IDs{indID}]);
   
    %% load _fMRIsessiondata.mat data to be altered 
   try
      a = load([pn.matpath, 'meanBOLD_ER_', IDs{indID}, '_v4.mat']);
   catch ME
      disp (ME.message)
      continue;
   end
   
   %% compute conditions 5,6,7,8,9,10
   % cond5 = cond4 - cond1
   a.st_datamat(5,:) = a.st_datamat(4,:)-a.st_datamat(1,:);
   % cond6 = cond34 - cond12
   a.st_datamat(6,:) = nanmean(a.st_datamat(3:4,:),1)-nanmean(a.st_datamat(1:2,:),1);
   % cond7 = cond234 - cond1
   a.st_datamat(7,:) = nanmean(a.st_datamat(2:4,:),1)-nanmean(a.st_datamat(1,:),1);
   % cond8 = 234
   a.st_datamat(8,:) = nanmean(a.st_datamat(2:4,:),1);
      
   % cond9 = add regression slope
%    for indVox = 1:size(a.st_datamat,2)
%        X = [1 1; 1 2; 1 3; 1 4];
%        b = regress(a.st_datamat(1:4, indVox), X);
%        a.st_datamat(9,indVox) = b(2);
%    end
   
   X = [1 1; 1 2; 1 3; 1 4];
   b=X\a.st_datamat(1:4, :);
   a.st_datamat(9,:) = b(2,:);
  
   %% edit conditions labels
   a.session_info.condition = cell(9,1);
   a.session_info.condition(1,1) = {'MeanLoad1'};
   a.session_info.condition(2,1) = {'MeanLoad2'};
   a.session_info.condition(3,1) = {'MeanLoad3'};
   a.session_info.condition(4,1) = {'MeanLoad4'};
   a.session_info.condition(5,1) = {'MeanLoad4min1'};
   a.session_info.condition(6,1) = {'MeanLoad34min12'};
   a.session_info.condition(7,1) = {'MeanLoad234min1'};
   a.session_info.condition(8,1) = {'MeanLoad234'};
   a.session_info.condition(9,1) = {'MeanRegress1234'};
   
   a.session_info.condition0 = a.session_info.condition;
     
   %% edit condition counters
   a.session_info.num_conditions  = 9;
   a.session_info.num_conditions0 = 9;
   
   %% add zero vector to st_datamat if condition(=session) not available
   % update condition variables according to count of conditions

   for indCond = size(a.session_info.condition_baseline,2)+1:9

       a.session_info.condition_baseline{indCond}  = a.session_info.condition_baseline{1};
       a.session_info.condition_baseline0{indCond} = a.session_info.condition_baseline0{1};

   end
       
   % num_subj_cond AND st_evt_list: ones or numbers = count of conditions
   a.num_subj_cond = [1,1,1,1,1,1,1,1,1];
   a.st_evt_list   = [1,2,3,4,5,6,7,8,9];

    %% save new data file
   save([pn.matpath, 'meanBOLD_ER_', IDs{indID}, '_BfMRIsessiondata.mat'], '-struct', 'a');
    
   clear a
   
end