function A3_createMeanBOLD_ER(ID)

% - create meanBOLD data matrices for PLS
% - regress out 6 DOF motion parameters, CSF and WM mean signals
% - censor significant DVARS volumes by spectral interpolation (Powers et al., 2014)

mode = 'local';
%mode = 'tardis';

if strcmp(mode, 'tardis')
    pn.root         = '/home/mpib/lndg/StateSwitch/WIP/';
    pn.coordpath    = [pn.root, 'B5_PLS_eventRelated/B_data/VoxelOverlap/'];
    pn.PLSfilesIn	= [pn.root, 'B5_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.PLSfilesOut	= [pn.root, 'B5_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.PreprocFiles = [pn.root, 'preproc/'];
    % The following toolboxes need to be added using the prepare script:
    % rs-fMRI-master
    % NIFTI_toolbox
    % preprocessing_tools
else
    restoredefaultpath;
    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
    pn.tools        = [pn.root, 'analyses/B_PLS/T_tools/'];  addpath(genpath(pn.tools));
    pn.coordpath    = [pn.root, 'analyses/B4_PLS_preproc2/B_data/VoxelOverlap/'];
    pn.PLSfilesIn	= [pn.root, 'analyses/B5_PLS_eventRelated/B_data/MeanBOLD_ER_PLS/'];
    pn.PLSfilesOut	= [pn.root, 'analyses/B5_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    % add Powers et al. scrubbing toolbox
    addpath(genpath([pn.root, 'analyses/B4_PLS_preproc2/T_tools/rs-fMRI-master']))
end

% % N = 44 YA + 53 OA;
% IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
%     '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
%     '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
%     '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
%     '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
%     '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
%     '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
%     '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
%     '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc

    disp(['Processing subject ', ID, '.']);

%% create the SDBOLD datamats

    % load subject's sessiondata file
    a = load([pn.PLSfilesIn, 'meanBOLD_ER_', ID, '_fMRIsessiondata.mat']);

    % load common coordinates
    load([pn.coordpath, 'coords_N95.mat'], 'final_coords');
    
    conditions=a.session_info.condition(1:numConds_raw);
    a = rmfield(a,'st_datamat'); %4*1220060
    a = rmfield(a,'st_coords');

    %replace fields with correct info.
    a.session_info.datamat_prefix   = (['meanBOLD_ER_', ID, '_v4']); % SD PLS file name; _Bfmrisessiondata will be automatically appended!
    a.st_coords = final_coords;     % constrain analysis to shared non-zero GM voxels
    a.pls_data_path = pn.PLSfilesOut;

    %% Load NIfTI file

    blockCount(1:4) = 0;
    for indRun = 1:a.session_info.num_runs

        %% Load NIfTI
        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        if strcmp(mode, 'tardis')
            fname = [a.session_info.run(indRun).data_path '/' a.session_info.run(indRun).data_files{:}];
        else
            fname = [a.session_info.run(indRun).data_path '/' a.session_info.run(indRun).data_files{:}];
        end
        
        if ~exist(fname) || isempty(a.session_info.run(indRun).data_files)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        img_backup = img;
        img = img_backup;
        % demean and detrend within run
        img = img-repmat(nanmean(img,2),1, size(img,2));
        img = detrend(img', 'linear')';
                
        % get average signal from WM and CSF masks

        WMmask = [pn.root, 'analyses/B4_PLS_preproc2/B_data/A_standards/tissuepriors/avg152T1_white_MNI_3mm.nii'];
        CSFmask = [pn.root, 'analyses/B4_PLS_preproc2/B_data/A_standards/tissuepriors/avg152T1_csf_MNI_3mm.nii'];
        [WMmask] = double(S_load_nii_2d(WMmask));
        WMsignal = img(WMmask==1,:); WMsignal = nanmean(WMsignal,1);
        [CSFmask] = double(S_load_nii_2d(CSFmask));
        CSFsignal = img(CSFmask==1,:); CSFsignal = nanmean(CSFsignal,1);

        % restrict signal of interest to final_coords         
        img = img(final_coords,:);
        
        %% regress out motion parameters
        
        % detrended regression estimates (Powers et al., 2014)
        % mean of voxels within CSF and WM masks, also demeaned
        
        % get FSL motion parameters (6 DOF)
        motionParamFile = [pn.root, 'analyses/B5_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/A_motionParams/',ID,'/',ID,'_sess-',num2str(indRun),'_motion_6dof.txt'];
        motionParams = load(motionParamFile);
        
        % get DVARS outliers
        load([pn.coordpath, 'S2_DVARS.mat'], 'DVARSout');
        DVARS_idx = find(strcmp(DVARSout.IDs, ID));
        Volumes2Censor = zeros(size(motionParams,1),1);
        Volumes2Censor(DVARSout.SignificantDVARS{DVARS_idx,indRun,1}) = 1;
        
        % demean motion parameters
        motionParams = motionParams-repmat(mean(motionParams,1),size(motionParams,1),1);
                
        % basically, build a linear model of regressor influence only using
        % good volumes, then apply this model also to bad volumes

        % add signal within 95% tissue probability CSF, WM masks
        
        % define regressor matrix
        X = [motionParams, WMsignal', CSFsignal'];
        % remove data and regressors that need to be scrubbed
        % (i) ???bad??? timepoints were censored from the regressors and BOLD data
        X_good = X; X_good(Volumes2Censor==1,:) = [];
        img_good = img; img_good(:,Volumes2Censor==1) = [];
        % (ii) the remaining ???good??? regressors were standardized (zero-mean, unit variance) and detrended
        X_good = zscore(X_good,[],1);
        X_good = detrend(X_good, 'linear');
        % (iii) a least-squares fit of ???good??? regressors to the ???good??? data generated beta values
        b_good=X_good\img_good'; % matrix implementation of multiple regression        
        % (iv) regressors from all timepoints (???good??? and ???bad???) underwent the same transformation that defined the ???good??? standardized regressors
        X_all = X;
        X_all = zscore(X_all,[],1);
        X_all = detrend(X_all, 'linear');
        % (v) the ???good??? betas were applied to regressors from all (???good??? and ???bad???) timepoints to generate modeled signal values at all timepoints
        AllPredict = X_all*b_good;
        % (vi) residuals were calculated for all timepoints as observed minus modeled BOLD values
        res = img-AllPredict';
        
%         figure; imagesc(zscore(res,[],2),[-1 1])
%         figure; imagesc(zscore(img,[],2),[-1 1])
        
        %% implement DVARS censoring (a la Powers et al., 2014)

        % performed via spectral interpolation (Powers et al., 2014; Parkes et al., 2018)
                
        censor.data = res';
        censor.t = ([1:size(img,2)]') * .645;
        censor.scrubmask = Volumes2Censor';
        censor.ofac = 8;
        censor.hifac = 1;
        
        voxbinsize = 100;
        voxbin = 1:voxbinsize:size(censor.data,2);
        voxbin = [voxbin size(censor.data,2)];

        data_surrogate = zeros(size(censor.data,1),size(censor.data,2));

        for v = 1:numel(voxbin)-1 % this takes huge RAM if all voxels
            fprintf(1, 'Processing voxel bin %u/%u\n', v,numel(voxbin)-1);
            data_surrogate(:,voxbin(v):voxbin(v+1)) = JP14_getTransform(censor.data(:,voxbin(v):voxbin(v+1)),censor.t,censor.scrubmask,censor.ofac,censor.hifac);
        end

        % insert surrogate data into real data at censored time points
        data_clean = censor.data;
        data_clean(censor.scrubmask==1,:) = data_surrogate(censor.scrubmask==1,:);
        data_clean = data_clean';
        
        %% plot exemplary influence of DVARS scrubbing (i.e. spectral interpolation)
%         scrm = censor.scrubmask;
%         scrm(scrm==0) = NaN;
% 
%         h = figure('units','normalized','position',[.1 .1 .7 .5]);
%         subplot(2,1,1); hold on; 
%             l1 = plot(censor.data(:,9901)', 'b', 'LineWidth', 2);
%             l2 = plot(data_surrogate(:,9901)', 'r', 'LineWidth', 2);
%             l3 = plot(data_clean(:,9901)', 'k', 'LineWidth', 2);
%             legend([l1, l2, l3], {'Original'; 'Spectral Reconstruction'; 'Cleaned signal'}); legend('boxoff');
%             xlim([1 1054])
%             title('Exemplary time-series censoring by spectral interpolation (Powers et al., 2014)')
%             ylabel('Amplitude')
%             xlabel('Time (TRs)');
%         subplot(2,1,2); hold on; 
%             plot(censor.data(:,9901)'-data_surrogate(:,9901)', 'k', 'LineWidth', 2)
%             sc = scatter(([1:size(img,2)]'), scrm, 100, 'filled', 'FillColor', 'r')
%             legend([sc], 'DVARS outliers'); legend('boxoff');
%             ylabel('Original-interpolated amplitude')
%             xlabel('Time (TRs)');
%             set(findall(gcf,'-property','FontSize'),'FontSize',24)
%             xlim([1 1054])       
        
%         %% DEBUG:
%         %%% DELETE THIS LINE AFTER DEBUGGING ####
%         data_clean = res;
%         %%%%

        %% extract relevant volumes

        pn.timing = [pn.root, 'analyses/A_extractDesignTiming/B_data/A_regressors/']; % location of info regarding run timing matrices

        % load timing information
        load([pn.timing, ID, '_Run',num2str(indRun),'_regressors.mat'], 'Regressors', 'RegressorInfo');
        
        block_data = NaN(4,size(data_clean,1),17);
        % encode in datamat
        for indCond = 1:numel(conditions)
            onsets = find(sum(Regressors(:,7:10),2)==indCond); % get onsets
            % encode data for each block            
            for indBlock = 1:numel(onsets)
            % TO DO: concatenate across runs!
                blockCount(indCond) = blockCount(indCond)+1;
                block_data_tmp(blockCount(indCond),:,:) = data_clean(:,onsets(indBlock):onsets(indBlock)+16); % encode 16 additional volumes
            end
        end

    end

%     figure; imagesc(squeeze(block_data(4,:,:))-squeeze(block_data(1,:,:)),[-100 100])
% 
%     % TO DO: average in thalamic network mask to investigate load effect

    % normalize or average across all blocks
    block_data(indCond,:,:) = squeeze(nanmean(block_data_tmp,1));
    % use alternative normalizations here?

    for indCond = 1:numel(conditions)
        curCondData = squeeze(block_data(indCond,:,:)); % 61003x17
        % reshape into PLS format
        a.st_datamat(indCond,:) = reshape(curCondData',[],1); %4*1220060
    end

    clear img;

    %% Save SD datamat
    save([pn.PLSfilesOut, a.session_info.datamat_prefix '.mat'],'-struct','a','-mat');
    disp ([ID ' done!'])
    
