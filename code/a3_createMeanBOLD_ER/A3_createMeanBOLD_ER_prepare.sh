#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% go to analysis directory containing .m-file
pn.rootDir = '/home/mpib/LNDG/StateSwitch/WIP/B6_PLS_eventRelated/A_scripts/E_v4/A3_createMeanBOLD_ER/';
cd([pn.rootDir])
%% compile function and append dependencies
mcc -m A3_createMeanBOLD_ER.m -a ./../../../T_tools/NIFTI_toolbox/ -a ./../../../T_tools/preprocessing_tools/ -a ./../../../T_tools/rs-fMRI-master/func/
exit