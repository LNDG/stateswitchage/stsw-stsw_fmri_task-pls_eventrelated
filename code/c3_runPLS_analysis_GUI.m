clear all; clc; restoredefaultpath

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/';
%pn.rootTardis      = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/';
pn.plsroot      = [pn.root, 'B6_PLS_eventRelated/'];
pn.plstoolbox   = [pn.root, 'B6_PLS_eventRelated/T_tools/pls/']; addpath(genpath(pn.plstoolbox));

cd([pn.plsroot, 'B_data/MeanBOLD_ER_PLS_v4/']);

% batch_plsgui('meancentPLS_ER_STSWD_YA_N44_3mm_1000P10.txt')
% batch_plsgui('behavPLS_ER_AMF_DDM_STSWD_YA_N44_3mm_1000P10.txt')
% 
% % model with spectral EEG PLS, HDDM vt intercept + change, 1/f, MSE, pupil 
% batch_plsgui('behavPLS_ER_fullModel_STSWD_YA_N42_3mm_1000P10.txt')

%batch_plsgui('behavPLS_ER_AMF_DDMdrift_STSWD_YA_N44_3mm_1000P10.txt')

plsgui
