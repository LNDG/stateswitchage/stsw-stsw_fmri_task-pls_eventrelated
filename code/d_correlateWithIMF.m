cd('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4')

%load('meancentPLS_ER_STSWD_YA_N44_3mm_1000P10_BfMRIresult.mat')
load('behavPLS_ER_AMF_DDM_STSWD_YA_N44_3mm_1000P10_fMRIresult.mat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% jointsubs = intersect(EEGAttentionFactor.IDs,IDs);
% attFctrIdx = find(ismember(EEGAttentionFactor.IDs,jointsubs));
% PLSIdx = find(ismember(IDs,jointsubs));

IDs=cellfun(@(x) x(13:16),subj_name,'un',0);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

%conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};
conds = {'regr1234'};

condData = []; uData = []; u2Data = [];
for indGroup = 1:1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
        %u2Data{indGroup}(indCond,:) = result.boot_result.usc2(targetEntries,1);
    end
end

figure;
bar(result.boot_result.orig_corr(:,1))
behavnames = {'EEG Modulation Factor'; 'NDT L1'; 'NDT Change'; 'Drift L1'; 'Drift Change'; 'Threshold L1'};
set(gca, 'Xtick', 1:6)
set(gca, 'xticklabels', behavnames)
ylabel('Correlation coefficient')


individualBrainScores = uData{1}';

% linear change in brainscores

X = [1 1; 1 2; 1 3; 1 4];
b=X\individualBrainScores';
linearBSchange = b(2,:);

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

jointsubs = intersect(EEGAttentionFactor.IDs,IDs);
attFctrIdx = find(ismember(EEGAttentionFactor.IDs,jointsubs));
PLSIdx = find(ismember(IDs,jointsubs));

figure; 
scatter(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), linearBSchange(PLSIdx), 'filled')
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), linearBSchange(PLSIdx))

figure; 
scatter(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,1:4),2), 'filled')
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,1:4),2))


figure; 
scatter(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,1),2), 75, 'k','filled')
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,1),2))


scatter(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,2:4),2)-individualBrainScores(PLSIdx,1), 'filled')
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,2:4),2)-individualBrainScores(PLSIdx,1))

% probe correlation with 1/f and parieto-occipital entropy

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_SlopeFits.mat', 'SlopeFits')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_stat.mat', 'stat')

jointsubs = intersect(SlopeFits.IDs,IDs);
attFctrIdx = find(ismember(SlopeFits.IDs,jointsubs));
PLSIdx = find(ismember(IDs,jointsubs));

% linear change in 1/f

X = [1 1; 1 2; 1 3; 1 4];
b=X\squeeze(nanmean(SlopeFits.linFit_2_30(:,:,find(stat.posclusterslabelmat)),3))';
linear1Fchange = b(2,:);

figure
scatter(nanmean(individualBrainScores(PLSIdx,2:4),2)-nanmean(individualBrainScores(PLSIdx,1),2),nanmean(nanmean(SlopeFits.linFit_2_30(attFctrIdx,2:4,find(stat.posclusterslabelmat)),3),2)-...
    nanmean(nanmean(SlopeFits.linFit_2_30(attFctrIdx,1,find(stat.posclusterslabelmat)),3),2), 'filled')
[r, p] = corrcoef(nanmean(individualBrainScores(PLSIdx,2:4),2)-nanmean(individualBrainScores(PLSIdx,1),2),nanmean(nanmean(SlopeFits.linFit_2_30(attFctrIdx,2:4,find(stat.posclusterslabelmat)),3),2)-...
    nanmean(nanmean(SlopeFits.linFit_2_30(attFctrIdx,1,find(stat.posclusterslabelmat)),3),2))

figure
scatter(nanmean(individualBrainScores(PLSIdx,1:4),2),nanmean(nanmean(SlopeFits.linFit_2_30(attFctrIdx,1:4,find(stat.posclusterslabelmat)),3),2), 'filled')
[r, p] = corrcoef(nanmean(individualBrainScores(PLSIdx,1:4),2),nanmean(nanmean(SlopeFits.linFit_2_30(attFctrIdx,1:4,find(stat.posclusterslabelmat)),3),2))

% behavioral PLS brainscore is uncorrelated with 1/f change

figure; 
scatter(linear1Fchange(attFctrIdx), nanmean(individualBrainScores(PLSIdx,1),2), 75, 'k','filled')
[r, p] = corrcoef(linear1Fchange(attFctrIdx), nanmean(individualBrainScores(PLSIdx,1),2))

% check correlation with neuromodulation during EEG

%% link to behavior, pupil etc...
 
	load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

    idx_IDs = find(ismember(STSWD_summary.IDs, jointsubs));
    
     h = figure('units','normalized','position',[.1 .1 .4 .4]);
    subplot(2,2,1);
        a = (squeeze(nanmedian(STSWD_summary.HDDMwSess.nondecisionEEG(idx_IDs,1),2)));
        b = individualBrainScores(PLSIdx,1);
        scatter(a, b, 'k','filled'); l1 = lsline();
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
        legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
        title('Baseline relation to NDT')
    subplot(2,2,2);
        a = (squeeze(nanmedian(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,1),2)));
        b = individualBrainScores(PLSIdx,1);
        scatter(a, b, 'k','filled'); l1 = lsline();
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
        legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
        title('Baseline relation to Drift')
    subplot(2,2,3);
        a = (squeeze(nanmedian(STSWD_summary.HDDMwSess.thresholdEEG(idx_IDs,1),2)));
        b = individualBrainScores(PLSIdx,1);
        scatter(a, b, 'k','filled'); l1 = lsline();
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
        legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
        title('Baseline relation to Threshold')
    subplot(2,2,4);
        a = (squeeze(nanmedian(STSWD_summary.pupil.stimPupilRelChange(idx_IDs,1),2)));
        b = individualBrainScores(PLSIdx,1);
        scatter(a, b, 'k','filled'); l1 = lsline();
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
        legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
        title('Baseline relation to pupil dilation')
       
h = figure('units','normalized','position',[.1 .1 .4 .5]);

subplot(2,2,1); cla; hold on;

        a = individualBrainScores(PLSIdx,1);
        b = squeeze(nanmean(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,1:4),2));
        scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));

        legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
            'location', 'SouthEast'); legend('boxoff')

        xlabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'}); ylabel('Drift (average across loads)');
        title('Relation to Drift')
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
	subplot(2,2,3); cla; hold on;

        a = individualBrainScores(PLSIdx,1);
        b = squeeze(nanmean(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,2:4),2))-STSWD_summary.HDDMwSess.driftEEG(idx_IDs,1);
        scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));

        legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
            'location', 'SouthEast'); legend('boxoff')

        xlabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'}); ylabel({'Change in drift rate';'(Loads 2:4 - Load1)'});
        title('Relation to drift modulation')
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
   

subplot(2,2,2); cla; hold on;

       a = individualBrainScores(PLSIdx,1);
       b = squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_IDs,1:4),2));
        scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
       [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
       legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
           'location', 'SouthEast'); legend('boxoff')        
       xlabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'}); ylabel({'Pupil dilation (average across loads)'});
       title('Relation to pupil dilation')
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       
subplot(2,2,4); cla; hold on;

       a = individualBrainScores(PLSIdx,1);
       b = squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_IDs,2:4),2))-STSWD_summary.pupil.stimPupilRelChange(idx_IDs,1);
       
       a(b<-.03) = []; % remove one outlier
       b(b<-.03) = [];
       scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
       legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
           'location', 'SouthEast'); legend('boxoff')        
       xlabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'}); ylabel({'Change in pupil dilation';'(Loads 2:4 - Load1)'});
       title('Relation to pupil modulation')
       set(findall(gcf,'-property','FontSize'),'FontSize',18)    
       
       %% WIP: relation to BOLD Dimensionality
       
h = figure('units','normalized','position',[.1 .1 .4 .5]);

subplot(2,2,1); cla; hold on;

        a = individualBrainScores(PLSIdx,1);
        b = squeeze(nanmean(STSWD_summary.PCAdim.dim(idx_IDs,17,1:4),3));
        scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));

        legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
            'location', 'SouthEast'); legend('boxoff')

        xlabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'}); ylabel('Drift (average across loads)');
        title('Relation to Drift')
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
        % Subjects with stronger upregulation of thalamocortical engagement
        % also have generally more segregated (!) network dynamics.
        
subplot(2,2,2); cla; hold on;

for indNetwork = 17

       a = individualBrainScores(PLSIdx,1);
       b = squeeze(nanmean(STSWD_summary.PCAdim.dim(idx_IDs,indNetwork,2:4),3))-STSWD_summary.PCAdim.dim(idx_IDs,indNetwork,1);

       scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
       legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
           'location', 'SouthEast'); legend('boxoff')        
       xlabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'}); ylabel({'Change in pupil dilation';'(Loads 2:4 - Load1)'});
       title('Relation to pupil modulation')
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
end


%% plot select associations

h = figure('units','normalized','position',[.1 .1 .4 .5]);
subplot(2,2,1); cla; hold on;

        a = -1.*individualBrainScores(PLSIdx,1);
        b = squeeze(nanmean(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,1:4),2));
        scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));

        legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
            'location', 'SouthEast'); legend('boxoff')

        xlabel({'Change Brainscore'}); ylabel('Drift (average across loads)');
        title('Relation to Drift')
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
subplot(2,2,2); cla; hold on;

        a = -1.*individualBrainScores(PLSIdx,1);
        b = squeeze(nanmean(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,2:4),2))-STSWD_summary.HDDMwSess.driftEEG(idx_IDs,1);
        scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));

        legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
            'location', 'SouthEast'); legend('boxoff')

        xlabel({'Change Brainscore'}); ylabel({'Change in drift rate';'(Loads 2:4 - Load1)'});
        title('Relation to drift modulation')
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
        ylim([-2.5 0])
   
subplot(2,2,3); cla; hold on;

       a = -1.*individualBrainScores(PLSIdx,1);
       b = EEGAttentionFactor.PCAalphaGamma(attFctrIdx);
       scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
       legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
           'location', 'SouthEast'); legend('boxoff')        
       xlabel({'Change Brainscore'}); ylabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'});
       title('Relation to EEG rhythm modulation')
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       
subplot(2,2,4); cla; hold on;

       a = -1.*individualBrainScores(PLSIdx,1);
       b = squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_IDs,2:4),2))-STSWD_summary.pupil.stimPupilRelChange(idx_IDs,1);
       
       a(b<-.03) = []; % remove one outlier
       b(b<-.03) = [];
       scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
       legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
           'location', 'SouthEast'); legend('boxoff')        
       xlabel({'Change Brainscore'}); ylabel({'Change in pupil dilation';'(Loads 2:4 - Load1)'});
       title('Relation to pupil modulation')
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       
        ylim([-0.02 0.04])
        
        pn.plotFolder = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B6_PLS_eventRelated/C_figures/';
        figureName = 'D_behavPLScorrVars';

        saveas(h, [pn.plotFolder, figureName], 'fig');
        saveas(h, [pn.plotFolder, figureName], 'epsc');
        saveas(h, [pn.plotFolder, figureName], 'png');

%% plot within single scatter plot

h = figure('units','normalized','position',[.1 .1 .15 .2]);
cla; hold on;

        a = -1.*individualBrainScores(PLSIdx,1);
        b = zscore(squeeze(nanmean(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,1:4),2)));

        ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', 'k', 'LineWidth', 3);

        scatter(a, b, 70, 'k','filled');
        [r1, p1] = corrcoef(a(~isnan(a)), b(~isnan(a)));
        xlabel({'Change Brainscore'}); ylabel('Drift (average across loads)');
       
        % add AMF modulation
        
       a = -1.*individualBrainScores(PLSIdx,1);
       b = zscore(EEGAttentionFactor.PCAalphaGamma(attFctrIdx));
        ls_2 = polyval(polyfit(a, b,1),a); ls_2 = plot(a, ls_2, 'Color', 'r', 'LineWidth', 3);

       scatter(a, b, 70, 'r','filled');
        [r2, p2] = corrcoef(a(~isnan(a)), b(~isnan(a)));
       xlabel({'Change Brainscore'}); ylabel({'Attentional modulation factor';'PC1 (Loads 2:4 - Load1)'});
       
% add pupil modulation

       a = -1.*individualBrainScores(PLSIdx,1);
       b = zscore(squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_IDs,2:4),2))-STSWD_summary.pupil.stimPupilRelChange(idx_IDs,1));
       
       a(b<-.03) = []; % remove one outlier
       b(b<-.03) = [];
        ls_3 = polyval(polyfit(a, b,1),a); ls_3 = plot(a, ls_3, 'Color', [.8 .8 .8], 'LineWidth', 3);

       scatter(a, b, 70, 'filled','MarkerFaceColor', [.8 .8 .8]);
        [r3, p3] = corrcoef(a(~isnan(a)), b(~isnan(a)));
       leg = legend([ls_1, ls_2, ls_3], {['Drift: r = ', num2str(round(r1(2),2)), ' p = ', num2str(round(p1(2),4))];...
           ['AMF: r = ', num2str(round(r2(2),2)), ' p = ', num2str(round(p2(2),4))]; ...
           ['Pupil: r = ', num2str(round(r3(2),2)), ' p = ', num2str(round(p3(2),4))]},...
           'location', 'SouthEast'); legend('boxoff')        
       xlabel({'Brainscore Change'}); ylabel({'Change in y [z-score]';'(Loads 2:4 - Load1)'});
       title('Brainscore relations')
       ylim([-4 3]); xlim([-800 1000])
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       set(findall(leg,'-property','FontSize'),'FontSize',14)
       
       pn.plotFolder = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B6_PLS_eventRelated/C_figures/';
        figureName = 'D_behavPLScorrVars_merged';

        saveas(h, [pn.plotFolder, figureName], 'fig');
        saveas(h, [pn.plotFolder, figureName], 'epsc');
        saveas(h, [pn.plotFolder, figureName], 'png');
        
        %% probe correlation with linear 1/f change [not significant]
        
        h = figure('units','normalized','position',[.1 .1 .15 .2]);
        cla; hold on;
        a =  nanmean(individualBrainScores(PLSIdx,1),2);
        b = linear1Fchange(attFctrIdx);
        scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
        set(l1, 'LineWidth', 3);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
        legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
            'location', 'SouthEast'); legend('boxoff')
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
