% create overall figure for all boostrap intervals

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B3_PLS_mean_v3/T_tools/NIFTI_toolbox/'];  addpath(genpath(pn.tools));
pn.dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/BSR_BehavPLS/';

for lag = 0:16
    curDataPath = [pn.dataPath, 'behavPLS_fullModel_bsr_lv1_lag',num2str(lag),'.img'];
    curData = load_nii(curDataPath);
    BSRdata(lag+1,:,:,:) = curData.img;
end

BSRdataPos = BSRdata;
BSRdataPos(BSRdataPos<3) = 0;

BSRdataNeg = BSRdata;
BSRdataNeg(BSRdataNeg>-3) = 0;

BSRmaxStim = squeeze(max(abs(BSRdataPos(9:14,:,:,:)),[],1));
BSRminStim = squeeze(max(abs(BSRdataNeg(9:14,:,:,:)),[],1));
BSRmeanStim = squeeze(mean(BSRdata(9:14,:,:,:),1));

% BSRmaxStim = squeeze(max(abs(BSRdataPos(1:16,:,:,:)),[],1));
% BSRminStim = squeeze(max(abs(BSRdataNeg(1:16,:,:,:)),[],1));
% BSRmeanStim = squeeze(mean(BSRdata(1:16,:,:,:),1));

%% save as separate images

% create individual Nifty file
tempNii = load_nii(curDataPath);
tempNii.img = BSRmaxStim;
save_nii(tempNii,[pn.dataPath, 'BSR_LV1_meanCent_maxStim.nii'])

tempNii.img = BSRminStim;
save_nii(tempNii,[pn.dataPath, 'BSR_LV1_meanCent_minStim.nii'])

tempNii.img = BSRmeanStim;
save_nii(tempNii,[pn.dataPath, 'BSR_LV1_meanCent_meanStim.nii'])