[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Event-related PLS & visualization

**Temporal dynamics of thalamic engagement.** To visualize the modulation of thalamic activity by load, we extracted signals within a binary thalamic mask extracted from the Morel atlas 114, including all subdivisions. Preprocessed BOLD timeseries were segmented into trials, spanning the period from the stimulus onset to the onset of the feedback phase. Given a time-to-peak of a canonical hemodynamic response function (HRF) between 5-6 seconds, we designated the 3 second interval from 5-8 seconds following the stimulus onset trigger as the stimulus presentation interval, and the 2 second interval from 3-5 s as the fixation interval, respectively. Single-trial time series were then temporally normalized to the temporal average during the approximate fixation interval. To visualize inter-individual differences in thalamic engagement (see Figure 7c), we performed a median split across participants based on their individual drift modulation. 

---

Note: event-related PLS was ultimately replaced by task PLS of SPM first-level files, but visualization was performed using the event-related PLS insputs

**f_**

* **Paper1** Visualize event-related changes in thalamus
* **Unreported** Visualize in V5, large-scale networks and thalamic nuclei
